package com.itdaima.modules.front.web;

import com.google.gson.JsonNull;
import com.itdaima.common.utils.FileUploadUtil;
import com.itdaima.common.utils.ImageUploadUtil;
import com.itdaima.common.utils.Result;
import com.itdaima.common.web.BaseController;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Administrator on 2017/7/19.
 */
@Controller
@RequestMapping(value="${frontPath}/upload")
public class UploadController extends BaseController {

    /**
         *图片上传
         * @param request
         * @param response
         * @param model
         * @return
         */
        @RequestMapping(value = {"uploadImage"})
        public String uploadImage(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
            String DirectoryName = "userfiles";
            try {
                ImageUploadUtil.ckeditor(request, response, DirectoryName);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         *文件上传
         * @return
         */
        @RequestMapping(value = {"uploadFile"})
        @ResponseBody
        public Result uploadFile(HttpServletRequest request,HttpServletResponse response) throws Exception {
            String DirectoryName = "files";
            String imageContextPath = null;
            try {
                imageContextPath = FileUploadUtil.ckeditor(request, response, DirectoryName);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Result.successResult().setObj(imageContextPath);

    }
}
