package com.itdaima.modules.front.entity;

import com.itdaima.common.persistence.DataEntity;
import com.itdaima.modules.sys.entity.User;

/**
 * Created by Administrator on 2017/8/2.
 * 对文章发表评论
 */
public class Comment extends DataEntity<Comment> {
    private static final long serialVersionUID = 1L;

    private String articleId; // 文章id
    private String content; // 评论类容
    private String replyUserName; // 回复用户id
    private User user; // 用户

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
