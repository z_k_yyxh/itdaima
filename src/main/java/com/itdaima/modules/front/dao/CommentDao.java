package com.itdaima.modules.front.dao;

import com.itdaima.common.persistence.CrudDao;
import com.itdaima.common.persistence.annotation.MyBatisDao;
import com.itdaima.modules.front.entity.Comment;

/**
 * Created by Administrator on 2017/8/2.
 */
@MyBatisDao
public interface CommentDao extends CrudDao<Comment> {


}
